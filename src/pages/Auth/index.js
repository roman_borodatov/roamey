import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInstagram,
  faGooglePlusG,
  faFacebookF
} from "@fortawesome/free-brands-svg-icons";
import styled from "styled-components";
import background from "../../assets/elevate-qqe1GRIt8TY-unsplash.jpg";

import SocialButton, {
  FacebookButton,
  GoogleButton,
  InstagramButton
} from "../../components/LoginSocialButtons";

const Page = styled.div`
  height: 100vh;
  background: url(${background}) rgba(0, 0, 0, 0.3);
  background-size: cover;
  background-blend-mode: multiply;
  background-repeat: no-repeat;
`;

const PageContainer = styled.div`
  height: 100%;
  max-width: 400px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  padding-bottom: 30px;
  box-sizing: border-box;
`;

const Title = styled.div`
  text-align: center;
  color: #fff;
  font-size: 36px;
  opacity: 0.6;
  margin-bottom: 15px;
`;

function Auth(props) {
  return (
    <Page>
      <PageContainer>
        <Title>Sign in</Title>
        <FacebookButton>
          <FontAwesomeIcon icon={faFacebookF} className="icon" />
          <span>connect with facebook</span>
        </FacebookButton>
        <GoogleButton>
          <FontAwesomeIcon icon={faGooglePlusG} className="icon" />
          <span>connect with google</span>
        </GoogleButton>
        <InstagramButton>
          <FontAwesomeIcon icon={faInstagram} className="icon" />
          <span>login with instagram</span>
        </InstagramButton>
      </PageContainer>
    </Page>
  );
}

Auth.propTypes = {};

export default Auth;

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import logo from "../../assets/Logo.png";

import Swipeable from "react-swipy";

import WishTile from "../../components/WishTile";
import image from "../../components/WishTile/yoga.jpg";
import image2 from "../../components/WishTile/photography.jpg";

const Page = styled.div`
  height: 100vh;
`;

const PageContainer = styled.div`
  height: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
`;

const LinksList = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  background-color: #fff;
`;

const Logo = styled.div`
  width: 50px;
  margin-top: 15px;
  img {
    width: 100%;
  }
`;

function WishList(props) {
  return (
    <Page>
      <PageContainer>
        <Header>
          <Logo>
            <img src={logo} alt="Logo" />
          </Logo>
        </Header>
      </PageContainer>
    </Page>
  );
}

WishList.propTypes = {};

export default WishList;

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import background from "../../assets/elevate-qqe1GRIt8TY-unsplash.jpg";
import logo from "../../assets/Logo.png";
import go from "../../assets/Go.png";

import MainLink from "../../components/common/MainLink";

const Page = styled.div`
  height: 100vh;
  background: url(${background}) rgba(0, 0, 0, 0.3);
  background-size: cover;
  background-blend-mode: multiply;
  background-repeat: no-repeat;
`;

const PageContainer = styled.div`
  height: 100%;
  max-width: 400px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 30px;
  box-sizing: border-box;
`;

const LinksList = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Logo = styled.div`
  width: 50px;
  margin-top: 15px;
  img {
    width: 100%;
  }
`;

const Go = styled.div`
  width: 100px;
  img {
    width: 100%;
  }
`;

function Example(props) {
  return (
    <Page>
      <PageContainer>
        <Logo>
          <img src={logo} alt="Logo" />
        </Logo>
        <LinksList>
          <MainLink title="Wishlist Title" />
          <MainLink title="Select a Destination" />
          <MainLink title="Select a Category" />
        </LinksList>
        <Go>
          <img src={go} alt="GO" />
        </Go>
      </PageContainer>
    </Page>
  );
}

Example.propTypes = {};

export default Example;

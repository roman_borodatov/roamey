import styled from "styled-components";

const SocialButton = styled.div`
  margin: 5px 0;
  font-family: "Montserrat", sans-serif;
  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.1);
  padding: 15px 15px;
  border-radius: 4px;
  text-transform: uppercase;
  font-weight: bold;
  text-decoration: none;
  transition: all 0.3s;
  width: 200px;
  font-size: 12px;

  .icon {
    width: 20px;
  }

  span {
    margin-left: 10px;
  }
`;

export const FacebookButton = styled(SocialButton)`
  background-color: #4267b2;
  color: #fff;
`;

export const GoogleButton = styled(SocialButton)`
  background-color: #cc3333;
  color: #fff;
`;

export const InstagramButton = styled(SocialButton)`
  background: #f09433;
  background: -moz-linear-gradient(
    45deg,
    #f09433 0%,
    #e6683c 25%,
    #dc2743 50%,
    #cc2366 75%,
    #bc1888 100%
  );
  background: -webkit-linear-gradient(
    45deg,
    #f09433 0%,
    #e6683c 25%,
    #dc2743 50%,
    #cc2366 75%,
    #bc1888 100%
  );
  background: linear-gradient(
    45deg,
    #f09433 0%,
    #e6683c 25%,
    #dc2743 50%,
    #cc2366 75%,
    #bc1888 100%
  );
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f09433', endColorstr='#bc1888',GradientType=1 );
  color: #fff;
`;

export default SocialButton;

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const MainLinkBackground = styled.div`
  width: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  margin: 5px 0;
  text-transform: uppercase;

  padding: 10px 0;
`;

function MainLink(props) {
  return <MainLinkBackground>{props.title}</MainLinkBackground>;
}

MainLink.propTypes = {};

export default MainLink;

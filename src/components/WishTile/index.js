import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Tile = styled.div`
  display: flex;
  flex: 1 1 300px;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  background-image: ${props => `url(${props.image})`};
  background-size: cover;
  background-repeat: no-repeat;
  height: 250px;
  padding: 10px 0;

  .first-row {
    align-self: flex-start;
    width: 100%;
  }

  .second-row {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
  }
`;

const TextDiv = styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  color: #fff;
  font-size: 18px;
  padding: 5px 10px;
  width: ${props => props.width};
`;

function WishTile(props) {
  return (
    <Tile image={props.image}>
      <div className="first-row">
        <TextDiv width="40%">{props.category}</TextDiv>
      </div>
      <div className="second-row">
        <TextDiv width="40%">{props.activityName}</TextDiv>
        <TextDiv width="20%">{props.distance}</TextDiv>
      </div>
    </Tile>
  );
}

export default WishTile;

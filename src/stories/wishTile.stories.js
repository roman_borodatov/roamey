import React from "react";
import styled from "styled-components";
import WishList from "../pages/Wishlist";

export default {
  title: "WishTile"
};

export const standard = () => <WishList />;
